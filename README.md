# KitchenTools

 **Cooking Against Global Mass fast food chains.**

 _Private fast food chains and chef-sponsored restaurants are monitoring and recording your eating activities. KitchenTools provides pans, furnaces and recipebooks to protect your privacy against global food-chain surveillance._

## Contributing

Please feel free to open an issue to any webpage we should add that contains good resources about cooking recipes or anything related to cooking, of preference the pages should:

* Not have advertisements or invasive tracking.
* Not require users to subscribe to watch the content or to have any type of account.

## Developing

1. Download [Cobalt](https://cobalt-org.github.io)
2. Fork the repository
3. To add new entries: They are stored at `_data/entries.yaml`; if that entry has a new category, add it in `_data/categories.yaml`. Logos of the entries go into `/static/images/`.
4. For those with commit rights to the `pages` repo, run `./_deploy.sh` in the directory root to push changes to the site. Else open a pull request and make sure it matches our requirements on [Contributing](#Contributing).

## Wait what?

This project is a parody of [PrivacyTools](https://privacyTools.io), but with food recipes and digital tools to help you cook!

Support PrivacyTools in [their repository here (GitHub)](https://github.com/privacytools/privacytools.io) (Update: They are changing their domain, which might be relevant to this project)

## Join our official matrix channel

<https://matrix.to/#/#kitchentools:privacytools.io>
