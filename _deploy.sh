#!/bin/sh
#
# Assumes and ya have push rights to the pages repo (duh)
# Note: doesn't remove old stuff

git submodule init
git submodule update
cd pages
git checkout master
cobalt build
git add -A
git commit -m "Synchronize site with new files; see KitchenTools/site for the specific changes"
git push
